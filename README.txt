DESCRIPTION:
------------
This module provides the settings as that is there in OG Access configurable for
each content type.
OG Access module in "Organic Groups" package has default settings to make the
new group posts public/private. Organic Group Access per content type uses the
same functions and provide interface to allow administrator to control this for
each content type.

The configurations can be found at
  Admistrator >> Organic groups access configuration by Content types

Dependancies:
  * Organic Groups
  * OG Access (OG Submodule)

INSTALLATION:
-------------
Installation:
1. Download and install Organic Groups and OG Access Modules.
2. Copy the extracted og_access_ct directory to your Drupal sites/all/modules
 directory.
3. Login as an administrator. Enable the module at 
 http://www.example.com/?q=admin/build/modules
4. Configure OG Visibility at http://www.example.com/?q=admin/og/og_access_ct.
